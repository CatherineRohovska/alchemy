//
//  UserProfile.m
//  Alchemy
//
//  Created by User on 11/5/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "UserProfile.h"
#define TimeStamp [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000]
@implementation UserProfile
+ (id)sharedProfileWithDelegate:(id<UserProfileDelegate>) delegate {
    static UserProfile *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] initWithDelegate:delegate ];
    });
    return sharedManager;
}
- (id)initWithDelegate:(id<UserProfileDelegate>) delegate
{
    if (self = [super init]) {
        self.delegate = delegate;
        static NSString* const hasRunAppOnceKey = @"hasRunAppOnce";
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];

        if ([defaults boolForKey:hasRunAppOnceKey] == NO)
        {
            _uid = TimeStamp;
            [defaults setObject:_uid forKey:@"UID_Alchemy"];
            if ([_delegate respondsToSelector:@selector(didFirstLaunch:)]) {
                [_delegate didFirstLaunch:self];
                [defaults setObject:_nickname forKey:@"Nickname_Alchemy"];
            }
            [defaults setBool:YES forKey:hasRunAppOnceKey];
        }
        else
        {
            _uid =(NSString*)[defaults objectForKey:@"UID_Alchemy"];
            _nickname =(NSString*)[defaults objectForKey:@"Nickname_Alchemy"];
        }
    }
    return  self;
}
-(void)updateName
{

        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject: self.nickname forKey:@"Nickname_Alchemy"];
        [defaults synchronize];
    
}
@end
