//
//  ElementCell.m
//  Alchemy
//
//  Created by User on 10/30/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "ElementCell.h"

@implementation ElementCell
{
    UILongPressGestureRecognizer *recognizer;
    UIImageView* img;
    UIView* layerView;
 
    
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _cellIsMoving = NO;
          layerView = [[UIView alloc] initWithFrame:self.bounds];
        [self.contentView addSubview:layerView];
        self.photoImageView = [[UIImageView alloc] initWithFrame:self.bounds];
       // self.photoImageView.contentMode  = UIViewContentModeScaleAspectFill;
        self.photoImageView.clipsToBounds = YES;
        self.photoImageView.backgroundColor = [UIColor blackColor];
        // self.photoImageView.translatesAutoresizingMaskIntoConstraints = NO;
       
        
        [layerView addSubview:self.photoImageView];
        self.name = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 20)];
        self.name.textColor = [UIColor whiteColor];
        //self.name.shadowColor = [[UIColor blackColor] colorWithAlphaComponent:0.7f];
       // self.name.shadowOffset = CGSizeMake(5, 5);
        [layerView addSubview:self.name];
    }
    self.name.text = @"NoName";
    return self;
}
- (void) createCell: (Element*) elem
{
  
    
    self.name.text = elem.name;
    NSMutableAttributedString *text =
    [[NSMutableAttributedString alloc]
     initWithAttributedString: self.name.attributedText];
    [text addAttribute:NSStrokeWidthAttributeName
                 value: @(-4.0)
                 range:NSMakeRange(0, _name.text.length)];
    [text addAttribute:NSStrokeColorAttributeName
                 value: [[UIColor cyanColor] colorWithAlphaComponent: 0.5f]
                 range:NSMakeRange(0, _name.text.length)];
    [self.name setAttributedText: text];
    self.element = elem;
    self.photoImageView.image = [UIImage imageNamed:elem.name];

    [layerView setUserInteractionEnabled:YES];

   
   // [img setUserInteractionEnabled:YES];
   
    // panRecognizer.delegate= self;
 

    
    

}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
     UITouch *touch = [touches anyObject];
     CGPoint position = [touch locationInView: self.superview.superview];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(cellTap:position:)])
    {
        [self.delegate cellTap:self position:position];
    }
//    
}
-(void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    //[super touchesMoved:touches withEvent:event];
    _cellIsMoving = YES;
    UITouch *touch = [touches anyObject];
    CGPoint position = [touch locationInView: self.superview.superview];
    if (self.delegate && [self.delegate respondsToSelector:@selector(cellMoved:cell:)])
    {
        [self.delegate cellMoved:position cell:self];
    }

}
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    if (self.delegate && [self.delegate respondsToSelector:@selector(cellDrop:)])
    {
        [self.delegate cellDrop:self];
    }
   // _cellIsMoving = NO;
}


@end
