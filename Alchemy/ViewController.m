//
//  ViewController.m
//  Alchemy
//
//  Created by User on 10/30/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "ViewController.h"
#import "ElementCell.h"
@import QuartzCore;
#import <QuartzCore/QuartzCore.h>
@interface ViewController ()
{
    AlchemyBook* book; //variable to get access to data storage of elements
    PairOfElements* pairCreation; //pair that user creates in game
    BOOL selected1, selected2; // flags that defines selection of elements in pair
    NSMutableArray* dataToShow; //data that shows in collectionView
    int selectedCount; // count of selected cells
    NSIndexPath* path; //position of cell in collectionView
    UIView* motionCell; // view that represents element graphically on drag
    UIView* preventionLayer;
    //pan gestures
    UIScreenEdgePanGestureRecognizer *edgePanGestureRecognizer; //to drag menu in
    UIPanGestureRecognizer *menuPanGestureRecognizer; //to drag menu out
    UserProfile* profile;
    BOOL isKeyboardMustHide;
}

@end

@implementation ViewController
-(BOOL)prefersStatusBarHidden
{
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
   // self.navigationController.toolbarHidden = YES;
    //self.navigationController.hidesBarsWhenVerticallyCompact = YES;
    isKeyboardMustHide = NO;
    // Do any additional setup after loading the view, typically from a nib.
    book = [AlchemyBook sharedBook];
    pairCreation = [PairOfElements alloc];
    [self.collectionElements registerClass:[ElementCell class] forCellWithReuseIdentifier:@"Cell"];
    selected1 = NO;
    selected2 = NO;
    selectedCount = 0;
    dataToShow = book.avaliableElements; //initialize list of avaliable elements
    [_finderField addTarget:self
                  action:@selector(editingChanged)
        forControlEvents:UIControlEventEditingChanged]; // catching event of editing field
    UIImageView* backgroundImage = [[UIImageView alloc] init];
    //creating background of view
    backgroundImage.frame = CGRectMake(backgroundImage.frame.origin.x, backgroundImage.frame.origin.y,self.view.bounds.size.height,self.view.bounds.size.height);
    backgroundImage.image =[UIImage imageNamed:@"background"];
    float angleInDegrees = 90; // change this value to what you want
    float angleInRadians = angleInDegrees * (M_PI/180);
    backgroundImage.transform = CGAffineTransformMakeRotation(angleInRadians);
    [self.view insertSubview:backgroundImage atIndex:0];
    //
 
       // creates gesture recognizer to drag menu in
edgePanGestureRecognizer = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(handleScreenEdgePanGesture:)];
    edgePanGestureRecognizer.edges = UIRectEdgeLeft;
    [self.view addGestureRecognizer:edgePanGestureRecognizer];
    //create gesture recognizer to grag menu out
menuPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
     [_menuView addGestureRecognizer:menuPanGestureRecognizer];
    preventionLayer = [[UIView alloc] initWithFrame: self.view.frame];
    preventionLayer.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    _menuView.backgroundColor =[[UIColor colorWithPatternImage:[UIImage imageNamed: @"menu_pattern"]] colorWithAlphaComponent:0.7f];
  //  [[UIColor blueColor] colorWithAlphaComponent:0.7f];
    
   
  
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //first load
    //profile.delegate = self;
    profile = [UserProfile sharedProfileWithDelegate:self];
}
- (void)handleScreenEdgePanGesture:(UIScreenEdgePanGestureRecognizer *)sender
{
   // NSLog(@" constraint %f",_edgeMenuConstraint.constant);
//if user drags menu and it's left side clips to left border of window
    if (_edgeMenuConstraint.constant<0&&[sender translationInView:self.view].x<=fabs(_edgeMenuConstraint.constant)) {

        _edgeMenuConstraint.constant = _edgeMenuConstraint.constant+[sender translationInView:self.view].x;
        menuPanGestureRecognizer.enabled = NO; //makes menu gesture recognizer disabled while it dragging
       
    }
    else
    {
        _edgeMenuConstraint.constant =0;
    }
    //if draging ends
    if (sender.state ==UIGestureRecognizerStateEnded) {
        // if menu showed less than a half
        if (_edgeMenuConstraint.constant<-_menuView.self.bounds.size.width/2){
            //make menu to hide
           
            [UIView animateWithDuration:0.25f animations:^{
            _edgeMenuConstraint.constant = -_menuView.self.bounds.size.width;
            
            }
             completion:^(BOOL finished) {
            [preventionLayer removeFromSuperview];
             }];
        }
        else{
         //make menu to show
            
            [UIView animateWithDuration:0.25f animations:^{
                _edgeMenuConstraint.constant = 0;
            }
             completion:^(BOOL finished) {
                 [self.view insertSubview:preventionLayer belowSubview:_menuView];
             }];
    
        }
        menuPanGestureRecognizer.enabled = YES; //set menu gesture recognizer enabled
    }
    [sender setTranslation:CGPointZero inView:self.view];
}
//menu gesture recognizer
- (void)handlePanGesture:(UIPanGestureRecognizer *)sender
{
  
   // if it is not opening and ending state of recognizer
    if (sender.state!=UIGestureRecognizerStateBegan && sender.state!=UIGestureRecognizerStateEnded)
    {
        //if velocity is negative (user drags menu to left side of screen)
        if ([sender velocityInView:self.view].x<0)
        {
          //makes menu slowly hide
            _edgeMenuConstraint.constant = _edgeMenuConstraint.constant+[sender translationInView:self.view].x;
          
            [sender setTranslation:CGPointZero inView:self.view];
            //if menu hides much further than full
            if (_edgeMenuConstraint.constant<-_menuView.self.bounds.size.width) {
                //makes it hide fully
                _edgeMenuConstraint.constant = -_menuView.self.bounds.size.width;
       
            }
            
        }
    }
    //if gesture ends
    if (sender.state ==UIGestureRecognizerStateEnded) {
        //if menu hides more than a half
        if (_edgeMenuConstraint.constant<-_menuView.self.bounds.size.width/2){
            //makes it to hide fully
            
            [UIView animateWithDuration:0.25f animations:^{
                _edgeMenuConstraint.constant = -_menuView.self.bounds.size.width;
            }
             completion:^(BOOL finished) {
                 [preventionLayer removeFromSuperview];
             }];
        }
        else{
            //makes it to show fully
           
            [UIView animateWithDuration:0.25f animations:^{
                _edgeMenuConstraint.constant = 0;
            }
             completion:^(BOOL finished) {
                  [self.view insertSubview:preventionLayer belowSubview:_menuView];
             }];
        }
    }
  
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //take notifications on event describes keyboard apperense and dissapearense
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    //adds tap detecting event to images that present selected cells
    UITapGestureRecognizer *twiceTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
    twiceTap.numberOfTapsRequired = 2;
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
    doubleTap.numberOfTapsRequired = 2;
    [_secondElementImage setUserInteractionEnabled:YES];
    [_secondElementImage addGestureRecognizer:twiceTap];
    [_firstElementImage setUserInteractionEnabled:YES];
    [_firstElementImage addGestureRecognizer:doubleTap];
    //makes menu to hide
    _edgeMenuConstraint.constant = 0-_menuView.bounds.size.width;
    //customize buttons
    UIFont* font = [UIFont fontWithName:@"Almendra SC" size:20];
    [_firstElementToCreation setFont:font];
    [_secondElementToCreation setFont:font];
    [_resetButton.titleLabel setFont:font];
    [_progressLabel setFont:font];
    [_greetingLabel setFont:font];
    [_creationButton.titleLabel setFont:font];
    [_hintButton.titleLabel setFont:font];
    [_changeNameButton.titleLabel setFont:font];
    [_reciepsBookButton.titleLabel setFont:font];
}
-(void)tapDetected: (UIGestureRecognizer*) gestureRecognizer{
    UIImageView* imView = (UIImageView*)gestureRecognizer.view;
    if (imView.center.x==_firstElementImage.center.x) {
        selected1 = NO;
        _firstElementImage.image = [UIImage imageNamed:@"cell_placeholder"];//nil;
        _firstElementToCreation.text = @"";
    }
    if (imView.center.x==_secondElementImage.center.x) {
        selected2 = NO;
        _secondElementImage.image = [UIImage imageNamed:@"cell_placeholder"];
        _secondElementToCreation.text = @"";
    }
    NSLog(@"%f", imView.center.x);
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
//keyboard events
- (void)keyboardWillShow:(NSNotification*)notification
{
    if (_finderField.isFirstResponder){
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
   // _topFieldConstraint.constant = _topFieldConstraint.constant-keyboardSize.height;
    CGRect newFrame = _collectionElements.frame;
    newFrame.size = CGSizeMake(_collectionElements.bounds.size.width, _collectionElements.bounds.size.height-keyboardSize.height);
    _collectionElements.frame = newFrame;
  //  [self.view layoutIfNeeded];
    _bottomFieldConstraint.constant = _bottomFieldConstraint.constant+keyboardSize.height;

    [UIView animateWithDuration:2.0
     
                        animations:^{
                         [self.view layoutIfNeeded];
                            _collectionElements.frame = newFrame;
                                            }
                     completion:nil
     ];
        //isKeyboardMustHide=YES;
    }
}

- (void)keyboardWillHide:(NSNotification*)notification
{
     if (_finderField.isFirstResponder){
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    //_topFieldConstraint.constant = _topFieldConstraint.constant+keyboardSize.height;
    CGRect newFrame = _collectionElements.frame;
    newFrame.size = CGSizeMake(_collectionElements.bounds.size.width, _collectionElements.bounds.size.height+keyboardSize.height);
    _collectionElements.frame = newFrame;
      
    _bottomFieldConstraint.constant = _bottomFieldConstraint.constant-keyboardSize.height;
            //isKeyboardMustHide = NO;
     }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//collection view
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return dataToShow.count;//counts data to show
}
//creating visible cell
- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    //create cell
    // [_imgColletion.collectionViewLayout invalidateLayout];
    ElementCell *cell = (ElementCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    //[cell deselectCell];
  
    [cell createCell:[dataToShow objectAtIndex:indexPath.item]];
    cell.backgroundColor = [UIColor redColor];
    if ([cell.name.text isEqualToString:_firstElementToCreation.text]||[cell.name.text isEqualToString:_secondElementToCreation.text]) {
       // [cell selectCell];
    }
    if (path==indexPath) {
        cell.hidden = YES;
    }
    cell.delegate = self;
    cell.exclusiveTouch = YES;
    UIFont* font = [UIFont fontWithName:@"Almendra SC" size:20];
    [cell.name setFont:font];
    return cell;
  //  self.
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}
//size of image cell
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //[_imgColletion.collectionViewLayout invalidateLayout];
//    double height = self.imgColletion.bounds.size.height;//self.view.bounds.size.height;
    CGSize size;
    size.width = 100;
    size.height = 100;
  
    return  size;   // return size;
}
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
      ElementCell* cell = ( ElementCell*)[collectionView cellForItemAtIndexPath:indexPath];
    if (cell.cellIsMoving) {
        cell.cellIsMoving = NO;
        return NO;
    }
    else
    {
     cell.cellIsMoving = NO;
        return YES;
        
    }
    
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //if we have selected cells
    selectedCount++;
    if (selectedCount>2) {
        selectedCount = 1;
    }

    ElementCell* cell = ( ElementCell*)[collectionView cellForItemAtIndexPath:indexPath];
    if (selected1 && selected2) {
        selected1 = NO;
        selected2 = NO;
     //   _secondElementImage.image = nil;
      //  _secondElementToCreation.text = @"";
        
    }
    if (selected1 && !selected2) {
        _secondElementToCreation.text = cell.name.text;//tmp.name;
        selected2 = YES;
        _secondElementImage.image = cell.photoImageView.image;
       // [cell selectCell];
        
    }
    if(!selected1) {
        _firstElementToCreation.text = cell.name.text;
        selected1 = YES;
        _firstElementImage.image = cell.photoImageView.image;
        //[cell selectCell];

    }
   
   
  
   // [collectionView reloadData];
}
//action of creating new element
- (IBAction)createNewElement:(id)sender {
 
    if (![_secondElementToCreation.text isEqualToString:@""] && ![_firstElementToCreation.text isEqualToString:@""]) {
        NSString* tmp = [[_firstElementToCreation.text stringByAppendingString:@"@"] stringByAppendingString:_secondElementToCreation.text];
        pairCreation = [pairCreation initWithString:tmp];
        Element* creationResult  = [book creation:pairCreation];
      if  (creationResult)
      {
          [_finderField resignFirstResponder];
          UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Congratulations!"
            message:[@"You discovered a new element: " stringByAppendingString:creationResult.name]
              preferredStyle:UIAlertControllerStyleAlert];
          UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        isKeyboardMustHide = YES;
                                       // [_finderField setValue:@YES forKey:@"isFirstResponder"];
                                     //adding created element to result if we show not all avaliable elements
                                        if (dataToShow.count<book.avaliableElements.count) {
                                            [dataToShow addObject:creationResult];
                                        }
                            
                                        //get last added path
                                       path = [NSIndexPath indexPathForItem:dataToShow.count-1 inSection:0];
                                        NSArray* array = [NSArray arrayWithObject:path];
                                        //insert new element
                                        [self.collectionElements insertItemsAtIndexPaths:array];
                                 path = [NSIndexPath indexPathForItem:dataToShow.count-1 inSection:0];
                                        //getting array of visible items
                                    NSArray* lastVisible = _collectionElements.indexPathsForVisibleItems;
                                        bool scroll  = NO;
                                    for (NSIndexPath* tmpVisible in lastVisible)
                                    {
                                        //if path is the same
                                        if (path.item==tmpVisible.item)
                                        { //create animation
                                        ElementCell* cell = (ElementCell*)[_collectionElements cellForItemAtIndexPath:[NSIndexPath indexPathForItem:dataToShow.count-1 inSection:0]];
                                        cell.hidden = YES;
                                            scroll = YES;
                                        CGPoint point =  [self.view convertPoint:cell.center fromView:cell.superview];
                                        [self animateCreation:cell.name.text position:point];
                                        break;
                                        }
                                    }
                                        if (!scroll) {
                                             [_collectionElements scrollToItemAtIndexPath:path atScrollPosition: UICollectionViewScrollPositionBottom animated:YES];
                                        }
                                   
                                                                
                                                                }];
          [alert addAction:defaultAction];
          
          //[alert addChildViewController:elementIcon];
          [self presentViewController:alert animated:YES completion:nil];
        
       //[_collectionElements reloadData];
          
      }
       // dataToShow = []
    }
    
 
    

}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    ElementCell* cell = (ElementCell*)[_collectionElements cellForItemAtIndexPath:[NSIndexPath indexPathForItem:dataToShow.count-1 inSection:0]];
    cell.hidden = YES;
   // [cell hideCell];
 
    CGPoint point =  [self.view convertPoint:cell.center fromView:cell.superview];
    [self animateCreation:cell.element.name position:point];
}
//working with finder text field
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    
    if (theTextField == self.finderField) {
        
        [theTextField resignFirstResponder];
        
    }
    
    return YES;
    
}
- (BOOL) textField: (UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString: (NSString *)string {
   
    if (!string.length)
    {
        return YES;
    }
    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXY"] invertedSet];
    
    if ([string rangeOfCharacterFromSet:set].location == NSNotFound) {
       // dataToShow = [NSMutableArra]
        return YES;
    }
    
    return NO;
}
-(void) editingChanged
{
    dataToShow = [[NSMutableArray alloc] init];
    for (int i=0; i<book.avaliableElements.count; i++) {
        // `Element* tmp = //[book.avaliableElements objectAtIndex:i];
        Element* tmp = [book.avaliableElements objectAtIndex:i];
        if ([tmp.name containsString:_finderField.text.lowercaseString]) {
            [dataToShow addObject:tmp];
            [_collectionElements reloadData];
            //return YES;
        }
    }
    if ([_finderField.text isEqualToString:@""]) {
        dataToShow = book.avaliableElements;
        [_collectionElements reloadData];
    }

}
-(void) animateCreation: (NSString*) elementName position: (CGPoint) position
{
    _collectionElements.userInteractionEnabled = NO;
    UIImage* imageElement = [UIImage imageNamed:elementName];
    UIImageView* creation = [[UIImageView alloc] initWithImage: imageElement];
    creation.alpha = 1.0;
    [UIView animateWithDuration:2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        _creationButton.enabled = NO;
    [creation setFrame:CGRectMake(0, 0, 100, 100)];
    creation.center = CGPointMake(self.view.bounds.size.width/2, _secondElementToCreation.center.y);
    [self.view addSubview:creation];
    float x = creation.layer.position.x;
    float y = creation.layer.position.y;
    float  w = position.x;
    float h = position.y;
    CGMutablePathRef thePath = CGPathCreateMutable();

    CGPathMoveToPoint(thePath,NULL,x,y);
    CGPathAddLineToPoint(thePath, NULL, w, h);
    
    CAKeyframeAnimation * theAnimation;
    
    // Create the animation object, specifying the position property as the key path.
    theAnimation=[CAKeyframeAnimation animationWithKeyPath:@"position"];
    
    theAnimation.path=thePath;
    theAnimation.duration=2.0;
        creation.alpha = 0.1;
    // Add the animation to the layer.
    [creation.layer addAnimation:theAnimation forKey:@"position"];
}
                    completion:^(BOOL finished){
                        creation.hidden = YES;
                        [creation removeFromSuperview];
                        ElementCell* cell = (ElementCell*)[_collectionElements cellForItemAtIndexPath:[NSIndexPath indexPathForItem:dataToShow.count-1 inSection:0]];
                        cell.hidden = NO;
                        path = nil;
                        _creationButton.enabled = YES;
                        _collectionElements.userInteractionEnabled = YES;
                    }];
    
}
//
-(void) cellTap:(ElementCell *)cell position:(CGPoint)position
{
    motionCell = [[UIView alloc] initWithFrame:cell.frame];
    motionCell.center = position;
    UIImageView* imgTmp = [[UIImageView alloc] initWithFrame:cell.photoImageView.frame];
    imgTmp.image = [UIImage imageNamed:cell.element.name];
     [motionCell addSubview:imgTmp];
    UILabel* lblTmp = [[UILabel alloc] initWithFrame:cell.name.frame];
        lblTmp.textColor = [UIColor whiteColor];
    
    UIFont* font = [UIFont fontWithName:@"Almendra SC" size:20];
    [lblTmp setFont:font];
    lblTmp.text = cell.element.name;
    [lblTmp setAttributedText:cell.name.attributedText];
     [motionCell addSubview:lblTmp];
    [self.view addSubview:motionCell];
        _collectionElements.scrollEnabled = NO;
    //cell.userInteractionEnabled = NO;
    
}
-(void) cellMoved: (CGPoint) position  cell:(ElementCell*) cell{
    motionCell.center = position;
  
  
}
- (void)cellDrop:(ElementCell *)cell
{
    if (motionCell.center.x>_firstElementImage.center.x-_firstElementImage.bounds.size.width/2 && motionCell.center.x<_firstElementImage.center.x+_firstElementImage.bounds.size.width/2 && motionCell.center.y>_firstElementImage.center.y - _firstElementImage.bounds.size.width/2 && motionCell.center.y<_firstElementImage.center.y+_firstElementImage.bounds.size.height/2)
    {
        _firstElementImage.image = [UIImage imageNamed:cell.element.name];
        _firstElementToCreation.text = cell.element.name;
        selected1 = YES;
        
    }
    if (motionCell.center.x>_secondElementImage.center.x-_secondElementImage.bounds.size.width/2 && motionCell.center.x<_secondElementImage.center.x+_secondElementImage.bounds.size.width/2 && motionCell.center.y>_secondElementImage.center.y - _secondElementImage.bounds.size.width/2 && motionCell.center.y<_secondElementImage.center.y+_secondElementImage.bounds.size.height/2)
    {
        _secondElementImage.image = [UIImage imageNamed:cell.element.name];
        _secondElementToCreation.text = cell.element.name;
        selected2=YES;
    }
    if (motionCell) {
        [motionCell removeFromSuperview];
    }
    _collectionElements.scrollEnabled = YES;
    //cell.userInteractionEnabled = YES;
   // cell.cellIsMoving = NO;
}

- (IBAction)toReciepesBook:(id)sender {
    [preventionLayer removeFromSuperview];
}

- (IBAction)showHint:(id)sender {
    int t=  [book cluesElementsCount];
    NSString* str = [NSString stringWithFormat:@"You can discover %d elements from avaliable ones.", t];
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Some clues..."
                                                                   message:str
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action) {
                                                            
                                                        }];
    
    
    [alert addAction:okAction];
 
    [self presentViewController:alert animated:YES completion:nil];


}

- (IBAction)resetProgress:(id)sender {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Reset all game progress"
                                                                   message:@"Are you really want to reset all progress?"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [book resetBook];
                                                              dataToShow = book.avaliableElements;
                                                              [_collectionElements reloadData];
                                                               _progressLabel.text = [NSString stringWithFormat:@"Unlocked: %lu/%lu", (unsigned long)book.avaliableElements.count, (unsigned long)book.allElements.count];
                                                          }];
    [alert addAction:defaultAction];
    UIAlertAction* closeAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                          
                                                          }];
    
  
    [alert addAction:closeAction];
    [self presentViewController:alert animated:YES completion:nil];
}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    _progressLabel.text = [NSString stringWithFormat:@"Unlocked: %lu/%lu", (unsigned long)book.avaliableElements.count, (unsigned long)book.allElements.count];
    _greetingLabel.text = [NSString stringWithFormat:@"Welcome, %@!", profile.nickname];
    _greetingLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _greetingLabel.numberOfLines = 0;
}
//user profile
- (void)didFirstLaunch:(UserProfile *)userProfile
{
   // NSString* str =@"Welcome to Alchemy!";
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Welcome to Alchemy!"
                                                                   message:@"Please, enter your name"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         UITextField* txt = [alert.textFields objectAtIndex:0];
                                                         if ([txt.text isEqualToString:@""]) {
                                                             profile.nickname = @"Unknown";
                                                         }
                                                         else{
                                                             profile.nickname =txt.text;
                                                         }
                                                         _greetingLabel.text = [NSString stringWithFormat:@"Welcome, %@!", profile.nickname];
                                                         _greetingLabel.lineBreakMode = NSLineBreakByWordWrapping;
                                                         _greetingLabel.numberOfLines = 0;
                                                     }];
    
    
    [alert addAction:okAction];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.delegate = self;
    }];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)didUpdateName:(UserProfile *)userProfile
{
    NSString* str = [NSString stringWithFormat:@"Dear %@, do you want to change name?", profile.nickname];
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:str                                                                   message:@"Please, enter your name"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         UITextField* txt = [alert.textFields objectAtIndex:0];
                                                         if ([txt.text isEqualToString:@""]) {
                                                             userProfile.nickname = profile.nickname;
                                                         }
                                                         else{
                                                             userProfile.nickname =txt.text;
                                                         }
                                                         _greetingLabel.text = [NSString stringWithFormat:@"Welcome, %@!", userProfile.nickname];
                                                         _greetingLabel.lineBreakMode = NSLineBreakByWordWrapping;
                                                         _greetingLabel.numberOfLines = 0;
                                                     }];
    
    
    [alert addAction:okAction];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
     [alert addAction:cancelAction];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.delegate = self;
    }];
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)changeName:(id)sender {
    NSString* str = [NSString stringWithFormat:@"Dear %@, do you want to change name?", profile.nickname];
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:str                                                                   message:@"Please, enter your name"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         UITextField* txt = [alert.textFields objectAtIndex:0];
                                                         if ([txt.text isEqualToString:@""]) {
                                                             profile.nickname = profile.nickname;
                                                             
                                                         }
                                                         else{
                                                             profile.nickname =txt.text;
                                                               [profile updateName];
                                                         }
                                                         _greetingLabel.text = [NSString stringWithFormat:@"Welcome, %@!", profile.nickname];
                                                         _greetingLabel.lineBreakMode = NSLineBreakByWordWrapping;
                                                         _greetingLabel.numberOfLines = 0;
                                                     }];
    
    
    [alert addAction:okAction];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:cancelAction];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.delegate = self;
    }];
    [self presentViewController:alert animated:YES completion:nil];
  
}

@end
