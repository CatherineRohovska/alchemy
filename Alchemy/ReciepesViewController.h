//
//  ReciepesViewController.h
//  Alchemy
//
//  Created by User on 11/6/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReciepesViewController : UIViewController
<UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIView *naviBar;
- (IBAction)backToGame:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableOfReciepes;

@end
