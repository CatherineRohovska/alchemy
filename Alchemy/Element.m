//
//  Element.m
//  Alchemy
//
//  Created by User on 10/30/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "Element.h"

@implementation Element
- (id) initElement: (NSString*) name reciepes: (NSMutableArray*) arrayOfReciepes
{
    if (self = [super init]) {
        _name = name;
        _reciepes = [[NSMutableArray alloc] init];
        for (int i=0; i<arrayOfReciepes.count; i++) {
            NSString* tmp = [arrayOfReciepes objectAtIndex:i];
            PairOfElements* newPair = [[PairOfElements alloc] initWithString:tmp];
            [_reciepes addObject:newPair];
        }
    }
    return self;
}
- (BOOL)isPair:(PairOfElements *) compared
{
    bool res = NO;
    for (int i=0; i<_reciepes.count; i++) {
        PairOfElements* tmp = [_reciepes objectAtIndex:i];
        
        res = [tmp isEqualToPair:compared];
        if (res) return res;
    }
    return res;
}
@end
