//
//  AlchemyBook.h
//  Alchemy
//
//  Created by User on 10/30/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Element.h"
@interface AlchemyBook : NSObject
@property (nonatomic, readonly) NSMutableArray* allElements;
@property (nonatomic, readonly) NSMutableArray* avaliableElements;
@property (nonatomic, readonly) NSMutableArray* discoveredReciepes;
+(id) sharedBook; //gain access to singleton
- (Element*) creation: (PairOfElements*) pair; //result of element creation
-(void) resetBook;
- (int) cluesElementsCount;
@end
