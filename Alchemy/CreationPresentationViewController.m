//
//  CreationPresentationViewController.m
//  Alchemy
//
//  Created by User on 11/6/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "CreationPresentationViewController.h"

@interface CreationPresentationViewController ()

@end

@implementation CreationPresentationViewController
-(BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UIImageView* backgroundImage = [[UIImageView alloc] init];
    //creating background of view
    backgroundImage.frame = CGRectMake(backgroundImage.frame.origin.x, backgroundImage.frame.origin.y,self.view.bounds.size.height,self.view.bounds.size.height);
    backgroundImage.image =[UIImage imageNamed:@"background"];
    float angleInDegrees = 90; // change this value to what you want
    float angleInRadians = angleInDegrees * (M_PI/180);
    backgroundImage.transform = CGAffineTransformMakeRotation(angleInRadians);
    [self.view insertSubview:backgroundImage atIndex:0];
    
    
    // Do any additional setup after loading the view.
    NSString *filePath = [[NSBundle mainBundle] pathForResource:_elementName ofType:@"gif"];
    NSData *gif = [NSData dataWithContentsOfFile:filePath];
    _animationViewer.backgroundColor = [UIColor redColor];
    
    [_animationViewer loadData:gif MIMEType:@"image/gif" textEncodingName:nil baseURL:nil];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIFont* font = [UIFont fontWithName:@"Almendra SC" size:20];
    [_backButton.titleLabel setFont:font];
    _naviBar.backgroundColor =[[UIColor colorWithPatternImage:[UIImage imageNamed: @"menu_pattern"]] colorWithAlphaComponent:0.9f];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
