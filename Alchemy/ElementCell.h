//
//  ElementCell.h
//  Alchemy
//
//  Created by User on 10/30/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Element.h"
@class ElementCell;
@protocol ElementCellDelegate<NSObject>
@required
-(void) cellMoved: (CGPoint) position  cell:(ElementCell*) cell;
@required
-(void) cellTap: (ElementCell*) cell position: (CGPoint) position;
@required
-(void) cellDrop: (ElementCell*) cell;
@end

@interface ElementCell : UICollectionViewCell
<UIGestureRecognizerDelegate>
@property(nonatomic, strong)  UIImageView *photoImageView; //represents image of element
@property(nonatomic, strong) Element *element; //element to which cell was created
@property(nonatomic, strong) UILabel *name; // represents element name
@property (nonatomic,strong) id <ElementCellDelegate> delegate; //to perform delegate
@property (nonatomic) bool cellIsMoving; //shows that cell is moving now
- (void) createCell: (Element*) elem;


@end
