//
//  AlchemyBook.m
//  Alchemy
//
//  Created by User on 10/30/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "AlchemyBook.h"

@implementation AlchemyBook
+ (id)sharedBook {
    static AlchemyBook *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init ];
    });
    return sharedManager;
}
-(id) init
{
    if (self = [super init]) {
    _allElements = [[NSMutableArray alloc] init];
    _avaliableElements = [[NSMutableArray alloc] init];
    _discoveredReciepes =[[NSMutableArray alloc] init];
    [self createReciepes];
    }
   // PairOfElements* pair = [[PairOfElements alloc] initWithString:@"fire@fire"];
  // Element* qwe = [self creation:pair];
     return self;
}
- (void) createReciepes
{
    NSError* err;
    NSString* dataString = [NSString stringWithContentsOfFile:[[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"alchemy_book"]
                                                     encoding:NSUTF8StringEncoding
                                                        error:&err];
    NSArray* reciepes =  [dataString componentsSeparatedByString:@"\n"];
    for (int i=0; i<reciepes.count; i++) {
        NSArray* singleElement = [reciepes[i] componentsSeparatedByString:@" "];
        NSMutableArray* reciepsTmp = [[NSMutableArray alloc] init];
        
        for (int j=1; j<singleElement.count; j++) {
            [reciepsTmp addObject:[singleElement objectAtIndex:j]];
            
        }
        Element* elementTmp = [[Element alloc] initElement:[singleElement objectAtIndex:0]  reciepes:reciepsTmp];
        [_allElements addObject:elementTmp];
        if ([elementTmp.name isEqualToString:@"fire"]||[elementTmp.name isEqualToString:@"water"]||[elementTmp.name isEqualToString:@"earth"]||[elementTmp.name isEqualToString:@"air"]) {
             if (![self isAvaliable:elementTmp]) {
            [_avaliableElements addObject:elementTmp];
             }
        }
        
    }
    
    [self avaliableFromFile];
}
-(void) resetBook
{
    //deleting discovered elements
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = paths.firstObject;
    NSArray* arrayToWrite = [[NSArray alloc] init];
    NSString* path = [[basePath stringByAppendingPathComponent:@"avaliable_book.plist"] stringByExpandingTildeInPath];
     [arrayToWrite writeToFile:path atomically:YES];
    path = [[basePath stringByAppendingPathComponent:@"discovered_reciepes.plist"] stringByExpandingTildeInPath];
    [arrayToWrite writeToFile:path atomically:YES];
    _avaliableElements = _avaliableElements = [[NSMutableArray alloc] init];
    _discoveredReciepes = [[NSMutableArray alloc]init];
    for (Element* elem in _allElements) {
        if ([elem.name isEqualToString:@"fire"]||[elem.name isEqualToString:@"water"]||[elem.name isEqualToString:@"earth"]||[elem.name isEqualToString:@"air"]) {
            if (![self isAvaliable:elem]) {
                [_avaliableElements addObject:elem];
            }
        }
    }
    
    
}
-(void) avaliableFromFile
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = paths.firstObject;
    NSString* path = [[basePath stringByAppendingPathComponent:@"avaliable_book.plist"] stringByExpandingTildeInPath];
    NSMutableArray* tmpArray = [NSMutableArray arrayWithContentsOfFile:path];
    for (int k =0; k<tmpArray.count; k++) {
        NSString* str = [tmpArray objectAtIndex:k];
        for (Element* tmp in _allElements) {
            if ([tmp.name isEqualToString:str]&&![self isAvaliable:tmp]) {
                [_avaliableElements addObject:tmp];
            }
        }
    }
    path = [[basePath stringByAppendingPathComponent:@"discovered_reciepes.plist"] stringByExpandingTildeInPath];
    _discoveredReciepes = [NSMutableArray arrayWithContentsOfFile:path];
    if (!_discoveredReciepes) {
        _discoveredReciepes = [[NSMutableArray alloc]init];
    }
}
- (Element*) creation: (PairOfElements*) pair
{
    Element* res = nil;
    BOOL success = NO;
    for (int i =0; i<_allElements.count; i++) {
        Element* tmp = [_allElements objectAtIndex:i];
        if ([tmp isPair:pair]){
            if (![self isAvaliable:tmp]) {
                res = tmp;
                NSString* reciepeToAdd = [[[[pair.firstElement stringByAppendingString:@" + "] stringByAppendingString:pair.secondElement] stringByAppendingString:@" = "] stringByAppendingString:res.name];
                [_discoveredReciepes addObject:reciepeToAdd];
                NSMutableArray* arrayToWrite = [[NSMutableArray alloc] init];
                [_avaliableElements addObject:tmp];
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *basePath = paths.firstObject;
                for (Element* elem in _avaliableElements) {
                    [arrayToWrite addObject:elem.name];
                }
                NSString* path = [[basePath stringByAppendingPathComponent:@"avaliable_book.plist"] stringByExpandingTildeInPath];
                success = [arrayToWrite writeToFile:path atomically:YES];
                path = [[basePath stringByAppendingPathComponent:@"discovered_reciepes.plist"] stringByExpandingTildeInPath];
                success = [_discoveredReciepes writeToFile:path atomically:YES];
                
            }
        }
    }
    return res;
}
- (BOOL) isAvaliable: (Element*) element
{
    bool res = NO;
    for (int i = 0; i<_avaliableElements.count; i++) {
        Element* tmp = [_avaliableElements objectAtIndex:i];
        if ([tmp.name isEqualToString:element.name]) {
            res = YES;
        }
    }
    return res;
}

- (int) cluesElementsCount{
    int newCanCreate = 0;
     for (int i =0; i<_allElements.count; i++) {
         Element* elemInAll = [_allElements objectAtIndex:i];
         for (Element* first in _avaliableElements) {
             for (Element* second in _avaliableElements) {
                 PairOfElements* pair = [[PairOfElements alloc] initWithString:[[first.name stringByAppendingString:@"@"] stringByAppendingString:second.name]];
                if([elemInAll.reciepes containsObject:pair]&&![self isAvaliable:elemInAll])
                {
                    newCanCreate++;
                }
             }
         }
     }
    return newCanCreate;
}
@end
