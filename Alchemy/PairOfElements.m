//
//  PairOfElements.m
//  Alchemy
//
//  Created by User on 10/30/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "PairOfElements.h"

@implementation PairOfElements
- (id) initWithString: (NSString*) formatedPair
{
    if (self = [super init]) {
        NSArray* pair = [formatedPair componentsSeparatedByString:@"@"];
        _firstElement = [[NSString alloc] init];
        _firstElement = pair[0];
        _secondElement = [[NSString alloc] init];
        _secondElement = pair[1];
    }
    return self;
}
-(BOOL) isEqualToPair: (PairOfElements*) compared
{
    bool res = NO;
    if ([compared.firstElement isEqualToString:self.firstElement]&&[compared.secondElement isEqualToString:self.secondElement]) {
        res = YES;
    }
    if ([compared.secondElement isEqualToString:self.firstElement]&&[compared.firstElement isEqualToString:self.secondElement]) {
        res = YES;
    }
    return res;
    
}
-(BOOL)isEqual:(id)object
{
    PairOfElements* compared = object;
    bool res = NO;
 
    if ([compared.firstElement isEqualToString:self.firstElement]&&[compared.secondElement isEqualToString:self.secondElement]) {
        res = YES;
    }
    return res;
}
@end
