//
//  Element.h
//  Alchemy
//
//  Created by User on 10/30/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PairOfElements.h"
@interface Element : NSObject
@property (nonatomic, readonly) NSString* name;
@property (nonatomic, readonly) NSMutableArray* reciepes;
- (BOOL) isPair:(PairOfElements *) compared;
- (id) initElement: (NSString*) name reciepes: (NSMutableArray*) arrayOfReciepes;
@end
