//
//  ReciepesViewController.m
//  Alchemy
//
//  Created by User on 11/6/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "ReciepesViewController.h"
#import "AlchemyBook.h"
#import "CreationPresentationViewController.h"
@interface ReciepesViewController ()
{
    AlchemyBook* book;
}

@end

@implementation ReciepesViewController
-(BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIImageView* backgroundImage = [[UIImageView alloc] init];
    //creating background of view
    backgroundImage.frame = CGRectMake(backgroundImage.frame.origin.x, backgroundImage.frame.origin.y,self.view.bounds.size.height,self.view.bounds.size.height);
    backgroundImage.image =[UIImage imageNamed:@"background"];
    float angleInDegrees = 90; // change this value to what you want
    float angleInRadians = angleInDegrees * (M_PI/180);
    backgroundImage.transform = CGAffineTransformMakeRotation(angleInRadians);
    [self.view insertSubview:backgroundImage atIndex:0];
    book = [AlchemyBook sharedBook];
}
-(void)viewWillAppear:(BOOL)animated
{   [super viewWillAppear:animated];
    UIFont* font = [UIFont fontWithName:@"Almendra SC" size:20];
    [_backButton.titleLabel setFont:font];
    _naviBar.backgroundColor =[[UIColor colorWithPatternImage:[UIImage imageNamed: @"menu_pattern"]] colorWithAlphaComponent:0.9f];
  
    
    _tableOfReciepes.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed: @"table_recieps_background"]];
    _tableOfReciepes.separatorStyle = 0;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return book.discoveredReciepes.count;
    //return dataStorage.devices.count;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"reciepeCell" forIndexPath:indexPath];
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.0f];
    cell.textLabel.text = [book.discoveredReciepes objectAtIndex:indexPath.item];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.textColor = [UIColor whiteColor];
    UIFont* font = [UIFont fontWithName:@"Dancing Script OT" size:30];
    //attributes
    NSMutableAttributedString *text =
    [[NSMutableAttributedString alloc]
     initWithAttributedString: cell.textLabel.attributedText];
    [text addAttribute:NSStrokeWidthAttributeName
                 value: @(-1.0)
                 range:NSMakeRange(0, cell.textLabel.text.length)];
    [text addAttribute:NSStrokeColorAttributeName
                 value: [[UIColor cyanColor] colorWithAlphaComponent: 0.5f]
                 range:NSMakeRange(0, cell.textLabel.text.length)];
    [cell.textLabel setAttributedText:text];
    [cell.textLabel setFont:font];
    return  cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CreationPresentationViewController* ctrl = [self.storyboard instantiateViewControllerWithIdentifier:@"creationViewController"];
    NSString* str = [book.discoveredReciepes objectAtIndex:indexPath.item];
    NSArray* strArray = [str componentsSeparatedByString:@" "];
    str = [strArray objectAtIndex:strArray.count-1];
    ctrl.elementName = @"animation";
 //   [self.navigationController pushViewController:ctrl animated:YES];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backToGame:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
