//
//  CreationPresentationViewController.h
//  Alchemy
//
//  Created by User on 11/6/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreationPresentationViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *naviBar;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
- (IBAction)backAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIWebView *animationViewer;
@property (weak, nonatomic) NSString* elementName;
@end
