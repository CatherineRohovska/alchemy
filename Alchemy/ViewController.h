//
//  ViewController.h
//  Alchemy
//
//  Created by User on 10/30/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlchemyBook.h"
#import "ElementCell.h"
#import "UserProfile.h"
@interface ViewController : UIViewController
<UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate, UIScrollViewDelegate,ElementCellDelegate, UserProfileDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionElements;
@property (weak, nonatomic) IBOutlet UILabel *firstElementToCreation;
@property (weak, nonatomic) IBOutlet UILabel *secondElementToCreation;
@property (weak, nonatomic) IBOutlet UIImageView *firstElementImage;
@property (weak, nonatomic) IBOutlet UIImageView *secondElementImage;
- (IBAction)createNewElement:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *finderField;
@property (weak, nonatomic) IBOutlet UIButton *creationButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topFieldConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomFieldConstraint;
@property (weak, nonatomic) IBOutlet UIView *menuView;
@property (weak, nonatomic) IBOutlet UIButton *reciepsBookButton;
@property (weak, nonatomic) IBOutlet UILabel *progressLabel;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;
@property (weak, nonatomic) IBOutlet UIButton *hintButton;
@property (weak, nonatomic) IBOutlet UIButton *changeNameButton;
- (IBAction)changeName:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *greetingLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *edgeMenuConstraint;
- (IBAction)toReciepesBook:(id)sender;
- (IBAction)showHint:(id)sender;
- (IBAction)resetProgress:(id)sender;
@end

