//
//  UserProfile.h
//  Alchemy
//
//  Created by User on 11/5/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UserProfile;
@protocol UserProfileDelegate <NSObject>
@required
-(void) didFirstLaunch: (UserProfile*) userProfile; //initiates name while first launch
@required
-(void) didUpdateName: (UserProfile*) userProfile; //to change name
@end
@interface UserProfile : NSObject
@property(nonatomic,strong) NSString* nickname; //user id
@property (nonatomic,readonly, strong) NSString* uid; // unique identyfier
@property (nonatomic,strong) id <UserProfileDelegate> delegate;
+(id) sharedProfileWithDelegate:(id<UserProfileDelegate>) delegate;
-(void) updateName;
@end
