//
//  PairOfElements.h
//  Alchemy
//
//  Created by User on 10/30/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PairOfElements : NSObject
@property (nonatomic, readonly) NSString* firstElement;
@property (nonatomic, readonly) NSString* secondElement;
- (id) initWithString: (NSString*) formatedPair;
-(BOOL) isEqualToPair: (PairOfElements*) compared;
@end
